# Coverage plots

Plots coverage files, SV calling and copy number for tumor/normal
samples

### Environment module ###

The tool is installed as a environment module in computerome.
To use the script installed in the module type the command:

```

module add tools R/3.2.1
module add cov_plot

```

The environment module can handle various version of the same
tool.

The path of the modulefiles are installed:

```
/home/projects/cu_10027/apps/modulefiles/cov_plot
```

The path of the cov_plot installations:

```
/home/projects/cu_10027/apps/software/cov_plot
```

### Pipeline in computerome ###

The `cov_plot` environment module is also included into the
`pype` command in computerome.

In order to use the cov_plot tools in the `pype` command
there is no need to previously load the `cov_plot` module:

```
module add pype
```

To use the tool:

```
pype snippets cov_plot
```
