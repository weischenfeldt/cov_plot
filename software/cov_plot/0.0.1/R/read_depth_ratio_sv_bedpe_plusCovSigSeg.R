#################################
# Plots the read depth ratio with sv arches from BEDPE format
# ------------------
# Run with:
# echo "dataFile=c('"tum.cov"','"germ.cov"'); svFile='"sv.bedpe"'; BAF='"baf.txt"'; sampleNames=c('"tumor"','"germ"')" | cat - read_depth_ratio_sv_bedpe_plusCovSigSeg.R | R --vanilla --slave
#
#################################

require("plotrix")

library("Cairo")
jpeg <- CairoJPEG

# Set default arguments
if (!exists("dataFile")) {
	dataFile=c('cov1','cov2');
}
if (!exists("chr")) {
	cov1 = read.table(dataFile[1]);
  cov1$V1 = sub(".fa", "", cov1$V1)
	chr = unique(cov1[,1])
}
if (!exists("maxRatio")) {
#	minRatio=-2.5;
	minRatio=-3;
	maxRatio=5;
}
if (!exists("minDelta")) {
  minDelta= 2;
}

if (!exists("vlines")) {
	vlines=c();
}
if (!exists("sampleNames")) {
	sampleNames=c('Tumor','Control');
}
if (!exists("Purity")) {
	Purity=1;
}
if (!exists("svFile")) {
  svFile='sv.txt';
	# Specifies the file containing the SVs in bedpe
}
if (!exists("scnaFile")) {
	x <- data.frame()
	write.table(x, file='readDepth.txt', col.names=FALSE)
	scnaFile='readDepth.txt';
	# Specifies the file containing the SCNA calls
}

if (!exists("suppPairsCutoff")) {
  suppPairsCutoff=2;
}

if (!exists("BAF")) {
        x <- data.frame()
        write.table(x, file='baf.txt', col.names=FALSE)
        bafFile='baf.txt';
        # Specifies the file containing the BAF calls
}

if (!exists("peakRegions")) {
  peakRegions='regions.txt';
	# Specifies the file containing the tab separated format
}


colorDel <- rgb(33, 102, 172, maxColorValue=255)
colorDup <- rgb(213, 62, 79, maxColorValue=255)
colorIns <- rgb(26, 152, 80, maxColorValue=255)
colorInv1 <- rgb(230, 140, 50, maxColorValue=255)
colorInv2 <- rgb(140, 81, 10, maxColorValue=255)
colorInter <- rgb(128, 50, 150, maxColorValue=255)


typeRD<-2
typeSV<-1

# Read SV files


#svs_full <- try(read.table(svFile, as.is=T, header=F))
regions <- try(read.table(peakRegions, as.is=T, header=F, sep='\t'))
if (class(regions)=='try-error') { regions <- data.frame() }
svs_full <- try(read.table(svFile, as.is=T, header=F, skip=1))

if (class(svs_full)=='try-error') { svs_full <- data.frame() }
rd_full <- try(read.table(scnaFile, as.is=T, header=T))
if (class(rd_full)=='try-error') { rd_full <- data.frame() }
baf_full <- try(read.table(BAF, as.is=T, header=T))
if (class(baf_full)=='try-error') { baf_full <- data.frame() }

svList <- data.frame()
svListTrans <- data.frame()
svListFiltered <- data.frame()
svListTransFiltered <- data.frame()


cov1_full = read.table(dataFile[1]);
cov1_full$V1 = sub(".fa", "", cov1_full$V1)
cov2_full = read.table(dataFile[2]);
cov2_full$V1 = sub(".fa", "", cov2_full$V1)
### Segmentation
if("DNAcopy" %in% rownames(installed.packages()) == FALSE) {install.packages("DNAcopy")}

require(DNAcopy)

cbind(cov1_full, LRR=log2(cov1_full$V4/cov2_full$V4))->cov_combi
cov_combi[cov_combi=="NaN" | cov_combi=="-Inf" | cov_combi=="Inf"]<-0
CNA.object <- CNA(cov_combi[,5],
                  cov_combi[,1],
                  cov_combi[,2],
                  data.type="logratio",sampleid=sampleNames[1])

# smoothing
smoothed.CNA.object <- smooth.CNA(CNA.object)

#Segmentation at default parameters
segment.smoothed.CNA.object <- segment(smoothed.CNA.object, verbose=1)

segment.smoothed.CNA.object$output->CNA.seg



# Iterate over chromosomes

#for(myChr in chr) {
#myChr='chr7'

pdf(paste(sampleNames[1], "_covplot.pdf", sep=""), width=18, height=10);
cov_plot_all <- dev.cur()

pdf(paste(sampleNames[1], "_covplot_sv.pdf", sep=""), width=18, height=10);
sv_plot_all <- dev.cur()
#dev.set(cov_plot_all)
#dev.set(sv_plot_all)

for(myChr in chr)
{
  if (grepl("GL|NC|hs37", myChr, perl=T))
  {
    next
  } else
    {cat ("chromosome:",myChr, "\n")


	# Read coverage files
	cov1 = cov1_full[cov1_full[,1]==myChr,]
	cov2 = cov2_full[cov2_full[,1]==myChr,]
	cov3 = CNA.seg[CNA.seg[,2]==myChr,]
	windowsMerged = 1

	print(paste("Chromosome:",myChr,"File:",dataFile[1],"Median:",round(median(cov1[,4])),"Mean:",round(mean(cov1[,4])),"Sd:",round(sd(cov1[,4])),sep=' '))
	print(paste("Chromosome:",myChr,"File:",dataFile[2],"Median:",round(median(cov2[,4])),"Mean:",round(mean(cov2[,4])),"Sd:",round(sd(cov2[,4])),sep=' '))

	# Get the chromosome wide median

    svs <- svs_full[(svs_full$V1==myChr | svs_full$V4==myChr) & svs_full$V8 >= suppPairsCutoff & svs_full$V14 > 0,]
    if (nrow(baf_full) >0) {
        baf_chrom <- baf_full[baf_full[,1]==myChr, ]
        cat("BAF stats", dim(baf_full), dim(baf_chrom), '\n')
    } else {
        baf_chrom <- data.frame()
    }
    if (nrow(rd_full) > 0){
	rdBf <- rd_full[rd_full[,1]==myChr, c('chromosome', 'start.pos', 'end.pos', 'Bf')]
	rdDup <- rd_full[rd_full[,1]==myChr & rd_full[,6]> 0.15 & rd_full[,8]>2,]
	rdDel <- rd_full[rd_full[,1]==myChr & rd_full[,6]< -0.15 & rd_full[,8]<2,]
	} else {
	rdDup = data.frame()
	rdDel = data.frame()
	      rdBf <- data.frame()
    }
    xnames <- cov1[,2];



    # Generate list of connections to be drawn
	ellipses <- data.frame()
	if (nrow(svs) > 0) {
		for (i in 1:nrow(svs)) {
      if (svs[i,1] == svs[i,4]){ # intrachrom
  			start <- svs[i,2]
  			end <- svs[i,5]
        if (svs[i,11] == "INS"){
      			# Here you can add further criteria to filter the shown SVs, e.g. require more supporting pairs in case the SV is within an amplified region:
      			# if ((median(logRatio[xnames<(start+10000) & xnames>(start-10000)], na.rm=T)>2 | median(logRatio[xnames<(end+10000) & xnames>(end-10000)], na.rm=T)>2) & dels[i,5]<5) next
      			ellipses <- rbind(ellipses, data.frame(start=start, end=end, color=colorIns, type=typeSV, stringsAsFactors=F, text1=paste("ins", start), text2=paste("ins", end)))
        }
        if (svs[i,11] == "DEL"){
      			# Here you can add further criteria to filter the shown SVs, e.g. require more supporting pairs in case the SV is within an amplified region:
      			# if ((median(logRatio[xnames<(start+10000) & xnames>(start-10000)], na.rm=T)>2 | median(logRatio[xnames<(end+10000) & xnames>(end-10000)], na.rm=T)>2) & dels[i,5]<5) next
      			ellipses <- rbind(ellipses, data.frame(start=start, end=end, color=colorDel, type=typeSV, stringsAsFactors=F, text1=paste("del", start), text2=paste("del", end)))
        }
        if (svs[i,11] == "DUP"){
      		# Here you can add further criteria to filter the shown SVs, e.g. require more supporting pairs in case the SV is within an amplified region:
    			# if ((median(logRatio[xnames<(start+10000) & xnames>(start-10000)], na.rm=T)>2 | median(logRatio[xnames<(end+10000) & xnames>(end-10000)], na.rm=T)>2) & dels[i,5]<5) next
    			ellipses <- rbind(ellipses, data.frame(start=start, end=end, color=colorDup, type=typeSV, stringsAsFactors=F, text1=paste("dup", start), text2=paste("dup", end)))
        }
        numInv1 <- 0
        numInv2 <- 0
        if (svs[i,11] == "INV"){
        			# Further filtering possible; see above
        			# Distinction between head-to-head and tail-to-tail inversion; They differ by their identifier
        			#if (grepl("Inversion_0", invs[i,7])) {
            if (svs[i,13] == "5to5") {
        				color <- colorInv1
        				numInv1 <- numInv1 + 1
        				type="head-to-head inversion"
					 typeName="h2h inv"
        			} else {
        				color <- colorInv2
        				numInv2 <- numInv2 + 1
        				type="tail-to-tail inversion"

					  typeName="t2t inv"
        			}
        			ellipses <- rbind(ellipses, data.frame(start=start, end=end, color=color,  type=typeSV, stringsAsFactors=F, text1=paste(typeName, start), text2=paste(typeName, end)))

        		}

      }
      if (svs[i,1] != svs[i,4]){ # interhrom

#        if (svs[i,1] == myChrNoFa) {

        if (svs[i,1] == myChr) {
      		start <- svs[i,2]
    			end <- svs[i,3]
  				otherChrPos <- paste(svs[i,4], ":", svs[i,5], sep="")
			} else {
  				start <- svs[i,5]
  				end <- svs[i,6]
  				otherChrPos <- paste(svs[i,1], ":", svs[i,2], sep="")
			}
			# Further filtering possible; see above
			ellipses <- rbind(ellipses, data.frame(start=start, end=end, color=colorInter, type=typeSV, stringsAsFactors=F, text1=paste("tlx",as.character(start),"->",otherChrPos), text2=""))
		}


		}
	}

	if (nrow(rdDel) > 0) {
		for (i in 1:nrow(rdDel)) {
			start <- rdDel[i,2]
			end <- rdDel[i,3]
			# Further filtering possible; see above
			ellipses <- rbind(ellipses, data.frame(start=start, end=end, color=colorDel, type=typeRD, stringsAsFactors=F, text1=paste("DEL", start), text2=paste("DEL", end)))
		}
	}


	if (nrow(rdDup) > 0) {
		for (i in 1:nrow(rdDup)) {
			start <- rdDup[i,2]
			end <- rdDup[i,3]
			# Further filtering possible; see above
			ellipses <- rbind(ellipses, data.frame(start=start, end=end, color=colorDup,type=typeRD, stringsAsFactors=F, text1=paste("DUP", start), text2=paste("DUP", end)))
		}
	}



	# Randomizing order of SVs, otherwise all lines representing inversions overlap deletions as well as duplications and so on
	ellipsesRand <- ellipses[sample(1:nrow(ellipses), nrow(ellipses), replace=F),]


	# Set window
	if (exists("win")) {
		# If necessary increase the window size
		covWinSize = cov1[1,3] - cov1[1,2]
		while (win[2] - win[1] < 10 * covWinSize) {
			win[1] = win[1] - 100;
			win[2] = win[2] + 100;
			if (win[1] < 0) {
				win[1] = 0;
			}
		}
	       	cov1 = cov1[cov1[,2] >= win[1],]
	       	cov1 = cov1[cov1[,3] <= win[2],]
	       	cov2 = cov2[cov2[,2] >= win[1],]
	       	cov2 = cov2[cov2[,3] <= win[2],]
		size1 = win[1] + 1100
		size2 = win[2] - 1100
	}
	xnames=cov1[,2];
	logRatio=log2(cov1[,4]/cov2[,4])
	globalMed=median(logRatio[is.finite(logRatio)])

	# Set title

	logCov1=log2(cov1[,4])
	logCov2=log2(cov2[,4])
	winSizeInKb=(cov1[1,3]-cov1[1,2])/1000 * windowsMerged

	sig <- pmax(cov1[,4],cov2[,4]) > 20

	ymax <- max(c(logCov1[is.finite(logCov1)], logCov2[is.finite(logCov2)]))

	if (length(logRatio[is.finite(logRatio)]) > 0) {

                dev.set(cov_plot_all)
		#pdf(paste(sampleNames[1], "_", myChr, "_covplot.pdf", sep=""), width=18, height=10);
		#postscript(paste(sampleNames[1], "_", myChr, "_zoomx2.eps", sep=""), width=1600, height=1000);

		par(mfrow=c(3,1))
		plot(xnames, logCov2, xlim=c(min(xnames), max(xnames)), ylim=c(0,ymax), main=paste(sampleNames[2], myChr), xlab="Chromosome position", ylab=paste("Log2 #read per ", winSizeInKb, "kb", sep=""), col='black', pch=20, cex.main=1.5, cex.axis=1.5, cex.lab=1.5, cex=0.8) #, xaxt="n")
		abline(h=median(logCov2[is.finite(logCov2)]), col=colors()[230], lty=1)
		abline(h=seq(0,(as.integer(ymax)+1),2), col="gray60", lty="dotted")
		abline(v=seq(min(xnames), max(xnames), 5000000), col="gray60", lty="dotted")
		plot(xnames, logCov1, xlim=c(min(xnames), max(xnames)), ylim=c(0,ymax), main=paste(sampleNames[1], myChr), xlab="Chromosome position", ylab=paste("Log2 #read per ", winSizeInKb, "kb", sep=""), col='black', pch=20, cex.main=1.5, cex.axis=1.5, cex.lab=1.5, cex=0.8) #, xaxt="n")
		abline(h=median(logCov1[is.finite(logCov1)]), col=colors()[230], lty=1)
		abline(h=seq(0,(as.integer(ymax)+1),2), col="gray60", lty="dotted")
		abline(v=seq(min(xnames), max(xnames), 5000000), col="gray60", lty="dotted")
	  	plot(xnames[sig], logRatio[sig], xlim=c(min(xnames), max(xnames)), ylim=c(min(logRatio[is.finite(logRatio)], -minDelta, na.rm = T),max(logRatio[is.finite(logRatio)], minDelta, na.rm = T)), main=paste("log2 ratio sample vs control", myChr), xlab="Chromosome position", ylab="Log2 ratio", col='black', pch=20, cex.main=1.5, cex.axis=1.5, cex.lab=1.5, cex=0.8) #, xaxt="n")

		abline(v=seq(min(xnames), max(xnames), 5000000), col="gray60", lty="dotted")
		abline(v=vlines, col="gray60", lty=1)
		abline(h=0, col=colors()[230], lty=1)

		abline(h=c(-20:-1,1:20), col="gray60", lty="dotted")

  segments(cov3$loc.start,cov3$seg.mean,cov3$loc.end,cov3$seg.mean,lwd=2, col="firebrick2")
	}
		#dev.off();


  xmin <- min(xnames)

	# Define y-axis plot range for read depth
	if (is.na(minRatio)) {
		ymin <- min(logRatio[is.finite(logRatio)])
	} else {
		ymin <- minRatio
	}
	if (is.na(maxRatio)) {
		ymax <- max(logRatio[is.finite(logRatio)])
	} else {
		ymax <- maxRatio
	}

	# Scaling for labels
	scaling <- 1


    dev.set(sv_plot_all)
    #pdf(paste(sampleNames[1], "_",myChr, "_covplot_sv.pdf", sep=""), width=18, height=10);
    # pdf(paste(sampleNames[1], "_",myChr, "_sv_zoom2.pdf", sep=""), width=1600, height=1000);
    #postscript(paste(sampleNames[1], "_", myChr, "_sv_zoomx2.eps", sep=""), width=1600, height=1000);
    if (nrow(baf_chrom) >0) {
        layout(matrix(c(3,2,1), 3, 1), heights=c(3,1,2))
    } else {
	layout(matrix(c(2,1), 2, 1), heights=c(3,2.5))
    }
		par(mar=c(7.2,6.1,0,2.1)*scaling);
    plot(xnames[sig], logRatio[sig], xlim=c(min(xnames), max(xnames)),
		#plot(xnames, logRatio, xlim=c(xmin, max(xnames)),
         ylim=c(ymin,ymax), main="", xlab="Chromosomal coordinates [Megabases]", ylab="Log2 ratio", col='black', pch=20, cex.main=1.5*scaling, cex.axis=2, cex.lab=2, cex=0.6*scaling, mgp=c(4, 2, 0)*scaling, xaxt="n")
		axis(1, cex.axis=2*scaling, cex.lab=2*scaling, mgp=c(4, 2, 0)*scaling, at=axTicks(1), labels=axTicks(1)/1000000)
  	segments(cov3$loc.start,cov3$seg.mean,cov3$loc.end,cov3$seg.mean,lwd=2, col="firebrick2")
  	abline(v=seq(min(xnames), max(xnames), 5000000), col="gray60", lty="dotted")
		abline(v=vlines, col="gray60", lty=1)
		abline(h=0, col=colors()[230], lty=1)
		abline(h=c(-20:-1,1:20), col="gray60", lty="dotted")

    if (nrow(rdDel) > 0){
    segments(rdDel[,2],log2(rdDel[,8]/2),rdDel[,3],log2(rdDel[,8]/2),lwd=max(4*(rdDel[,7]/min(rd_full[,7])), 2), col="darkorange")
    }
    if (nrow(rdDup) > 0){
    segments(rdDup[,2],log2(rdDup[,8]/2),rdDup[,3],log2(rdDup[,8]/2),lwd=max(4*(rdDup[,7]/min(rd_full[,7])) ,2), col="darkolivegreen4")
    }

    if (nrow(baf_chrom) >0) {
        plot(xnames[sig], logRatio[sig], type="n", xlim=c(min(xnames), max(xnames)),
            ylim=c(0, 1), main="", xlab="Chromosomal coordinates [Megabases]", ylab="BAF", col='black',
            pch=20, cex.main=1.5*scaling, cex.axis=2, cex.lab=2, cex=0.6*scaling, mgp=c(4, 2, 0)*scaling, xaxt="n")
	axis(1, cex.axis=2*scaling, cex.lab=2*scaling, mgp=c(4, 2, 0)*scaling, at=axTicks(1), labels=axTicks(1)/1000000)
  	abline(v=seq(min(xnames), max(xnames), 5000000), col="gray60", lty="dotted")
	abline(v=vlines, col="gray60", lty=1)
	abline(h=0.5, col="gray60", lty="dotted")


	segments(baf_chrom[, 2],baf_chrom[, 4], baf_chrom[, 3], baf_chrom[, 4], col="black", lwd = 5)
	segments(baf_chrom[, 2],1 - baf_chrom[, 4], baf_chrom[, 3], 1 - baf_chrom[, 4], col="black", lwd = 5)
    }


  # Plot SVs in top panel


	#dev.off()
	  # Plot SVs in top panel
	par(mar=c(0,6.1,4,2.1)*scaling)
		plot(0,-10, xlim=c(xmin, max(xnames)), ylim=c(0,2.5), axes=F, xlab="", ylab="",
         main=paste("log2 ratio", sampleNames[1], "vs", sampleNames[2], "-", myChr), cex.main=1.5*scaling)
		if (nrow(ellipsesRand) > 0) {
			for (i in 1:nrow(ellipsesRand)) {
				start <- ellipsesRand[i,"start"]
				end <- ellipsesRand[i,"end"]
				color <- ellipsesRand[i,"color"]
				type <- ellipsesRand[i,"type"]
				draw.ellipse((end-start)/2 + start, 0, (end-start)/2, 1, segment=c(0,180), lty=type,border=color,  lwd=2)
				text(start,runif(1,min=1,max=2.5), ellipsesRand[i,"text1"], pos=4, offset=0, cex=0.8, xpd=NA)
				text(end,runif(1,min=1,max=2.5), ellipsesRand[i,"text2"], pos=4, offset=0, cex=0.8, xpd=NA)
			}
			abline(h=0, col="white", lwd=5)
	}
    #dev.off()
  }

}

dev.off()
dev.off()

write.table(CNA.seg,file=paste(sampleNames[1], "_segmentation.txt", sep=""), col.names=T, row.names=F, sep="\t", quote=F)
